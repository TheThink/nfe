<?php
// 100% - Rohprotein% - Rohfett% - Rohasche% - Rohfaser% - Wasser% = NfE%.
$nfeCalculator = new nfeCalculater();

$nfeFileData = array();
$productKey = NULL;
if (!isset($_GET['submit']) && isset($_GET['key']) && $_GET['key'] != '') {
	$nfeCalculator->preLoadProductIfAlreadyCalculated((int) $_GET['key']);
} elseif (isset($_POST['submit']) && $_POST['submit'] == 'submit') {
	if (!$nfeCalculator->validRequest($_POST)) {
		print 'Check missing data';
		print $nfeCalculator->getForm();
		exit;
	}
	$nfeCalculator->preLoadProductIfAlreadyCalculated();
	$nfeCalculator->putCalculatedDataToFile();
}
print $nfeCalculator->getForm();
print $nfeCalculator->returnFormatedData();
exit;



class nfeCalculater {
	protected $dataFile = 'nfeData.php';
	protected $nfeFileData = array();
	protected $existingNfeEntry = array();
	protected $formData = array();
	protected $selectedProductId = NULL;

	protected $validDataKeys = array(
		'name',
		'rohprotein',
		'rohfett',
		'rohasche',
		'rohfaser',
		'feuchtigkeit'
	);

	protected $form = <<<FROM
	<html>
		<head>
			<meta name=viewport content="width=device-width, initial-scale=1">
		</head>
		<body>
		<style>
			label {
				display: block;
				font-weight: 600;
			}
			input[type=text] {
				width: 500px;
				border: 2px solid lightgrey;
				margin-bottom: 10px;
			}
			@media screen and (max-width: 980px) {
				input {
					width: 90%!important;
					height: 30px;
					margin-bottom: 10Px;
				}
			}
		</style>
		<form method="POST" action="nfe.php">
			<label for="name">Name:</label><input type="text" name="name" value="{name}" id="name"><br />
			<label for="rohprotein">Rohprotein:</label><input type="text" name="rohprotein" value="{rohprotein}" id="rohprotein"><br />
			<label for="rohfett">Rohfett:</label><input type="text" name="rohfett" value="{rohfett}" id="rohfett"><br />
			<label for="rohasche">Rohasche:</label><input type="text" name="rohasche" value="{rohasche}" id="rohasche"><br />
			<label for="rohfaser">Rohfaser:</label><input type="text" name="rohfaser" value="{rohfaser}" id="rohfaser"><br />
			<label for="feuchtigkeit">Feuchtigkeit:</label><input type="text" name="feuchtigkeit" value="{feuchtigkeit}" placeholder="81" id="feuchtigkeit"><br />
			<br /><input type="submit" name="submit" value="submit" />&nbsp;&nbsp;&nbsp;<a href="nfe.php" target="_self">clear</a>
		</form>
		</body>
	</html>
FROM;

	/**
	 * @return array
	 */
	public function getNfeData() {
		return $this->nfeFileData;
	}

	/**
	 * @return array
	 */
	public function getExistingNfeEntry() {
		return $this->existingNfeEntry;
	}

	/**
	 * @return string
	 */
	public function getForm() {
		$this->replaceFormMarker();
		return $this->form;
	}

	public function __construct() {
		// Pre load data and create data file if not exits.
		if (!$this->readDataFile()) {
			$handle = fopen('nfeData.php', 'w');
			fclose($handle);
		}
	}

	/**
	 * @return bool
	 */
	public function readDataFile() {
		if (!file_exists($this->dataFile)) {
			return FALSE;
		}
		$this->nfeFileData = json_decode(file_get_contents($this->dataFile), TRUE);
		return TRUE;
	}

	/**
	 * Collects the calucalated datat and call the method which write them to file.
	 *
	 * @return void
	 */
	public function putCalculatedDataToFile() {
		$nfeResults = $this->calculate();
		$nfeFileData = $this->nfeFileData;
		$_POST['name'] = preg_replace("!([\b\t\n\r\f\"\\'])!", "\\\\\\1", $_POST['name']);
		$_POST['name'] = urlencode($_POST['name']);
		$dataEntry = array(
			'name' => $_POST['name'],
			'calculatedNfe' => $nfeResults['calculatedNfe'],
			'calculatedDryWeight' => $nfeResults['calculatedDryWeight'],
			'calculatedNfeValueInDryWeight' => $nfeResults['calculatedNfeValueInDryWeight'],
			'inputs' => $_POST
		);
		if (!empty($this->existingNfeEntry)) {
			$nfeFileData[$this->selectedProductId] = $dataEntry;
		} else {
			$nfeFileData[] = $dataEntry;
		}
		$this->writeFile($nfeFileData);
	}

	/**
	 * @param $data
	 * @return void
	 */
	protected function writeFile($data) {
		if (!$string = json_encode($data)) {
			print 'Error on creating json string';
			exit;
		}
		$handle = fopen('nfeData.php', 'w');
		fwrite($handle, $string);
		fclose($handle);
	}

	/**
	 * Checks and preloads the data coming from file if them exists.
	 *
	 * @param null $selectedKey
	 * @return void
	 */
	public function preLoadProductIfAlreadyCalculated($selectedKey = NULL) {
		if (empty($this->nfeFileData)) {
			return;
		}
		if ((int) $selectedKey !== $selectedKey) {
			$selectedKey = NULL;
		}

		foreach ($this->nfeFileData as $key => $data) {
			$isSelectedProduct = !is_null($selectedKey) && (int) $selectedKey === (int) $key;
			$isExistingProduct = !empty($this->formData['name']) && $data['name'] == $this->formData['name'];
			if ($isSelectedProduct || $isExistingProduct) {
				$this->selectedProductId = (int) $key;
				$this->existingNfeEntry = $data['inputs'];
				$this->existingNfeEntry['name'] = urldecode($this->existingNfeEntry['name']);
				break;
			}
		}
	}

	/**
	 * Checks the incoming form data keys and values to make sure that all fields are set.
	 *
	 * @param array $incomingData
	 * @return bool
	 */
	public function validRequest(array $incomingData) {
		$validRequest = TRUE;
		foreach ($this->validDataKeys as $key) {
			if (empty($incomingData[$key])) {
				$validRequest = FALSE;
				break;
			}
			$this->formData[$key] = $incomingData[$key];
		}
		return $validRequest;
	}

	/**
	 * Replace all data in form with the file or input data.
	 *
	 * @return void
	 */
	protected function replaceFormMarker() {
		$this->form = str_replace(array('{name}', '{rohprotein}', '{rohfett}', '{rohasche}', '{rohfaser}', '{feuchtigkeit}'), $this->existingNfeEntry, $this->form);
	}

	/**
	 * Calculate the values by using the formdata.
	 *
	 * @return array
	 */
	protected function calculate() {
		$nfeResult = 100 - (float)str_replace(',', '.', $this->formData['rohprotein']) - (float)str_replace(',', '.', $this->formData['rohfett']) - (float)str_replace(',', '.', $this->formData['rohasche']) - (float)str_replace(',', '.', $this->formData['rohfaser']) - (float)str_replace(',', '.', $this->formData['feuchtigkeit']);
		$dryWeight = 100 - (int)$this->formData['feuchtigkeit'];
		return $nfeResults = array(
			'calculatedNfe' => $nfeResult,
			'calculatedDryWeight' => $dryWeight,
			'calculatedNfeValueInDryWeight' => $nfeResult / ($dryWeight / 100)
		);
	}

	/**
	 * Return all the file data to a nice output.
	 *
	 * @return string
	 */
	public function returnFormatedData() {
		$this->readDataFile();
		$outputLine = '';
		if (!empty($this->nfeFileData)) {
			foreach ($this->nfeFileData as $key => $nfeData) {
				$nfeData['calculatedNfeValueInDryWeight'] = round($nfeData['calculatedNfeValueInDryWeight'], 2);
				if ($nfeData['calculatedNfeValueInDryWeight'] <= 10) {
					$color = 'green';
				} else {
					$color = 'red';
				}
				$name = str_replace('|', urldecode($nfeData['name']), '<span style="color: ' . $color . '; font-weight: bold;">|</span>');
				$calculatedNfeValueInDryWeight = str_replace('|', $nfeData['calculatedNfeValueInDryWeight'], '<span style="color: ' . $color . '; font-weight: bold;">|</span>');
				$outputLine .= '<a href="?key=' . $key . '">' . $name . '</a><br /><strong>NfE-Wert (berechnet): </strong>' . $nfeData['calculatedNfe'] . '<br /><strong>Trockenmasse (berechnet): </strong>' . $nfeData['calculatedDryWeight'] . '<br /><strong>NfE-Wert in Trockenmasse (berechnet): </strong>' . $calculatedNfeValueInDryWeight . '<br /><br />';
			}
			return $outputLine;
		}
	}
}
?>