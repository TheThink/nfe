# NfE Calculator - Speichert zudem eingegebene Produkte#
### Short: ###
Mit Hilfe der Weender Analyse wird der Anteil der Kohlenhydrate in Nassfutter berechnet

### Long: ###
Mit diesem Tool kann man den NfE Wert von Katzenfutter (Nassfutter) berechnen. Experten empfehlen für Diabetes-Katzen, einen NfE-Anteil in der Trockenmasse, der unter 10 liegt.

Das Tool speichert alle eingegebenen Daten in einer Datei und zeigt Sie farbig an. Grün ist dabei für die Katze unbedenklich, rot ist nicht zu empfehlen.

Eine Auflistung einiger aktuell zu kaufenden Produkte befindet sich bereits in der nfeData Datei.

### Setup ###
Beide Dateien auf den Webserver in ein gewünschtes Verzeichnis legen. Sicherstellen, das die nfe.php Schreibrechte in dem Verzeichnis besitzt. Danach ist das Tool über /nfe.php im Browser aufrufbar.